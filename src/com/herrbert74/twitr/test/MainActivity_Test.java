package com.herrbert74.twitr.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.Smoke;

import com.herrbert74.twitr.activities.MainActivity;
import com.jayway.android.robotium.solo.Solo;

public class MainActivity_Test extends ActivityInstrumentationTestCase2<MainActivity> {

	private Solo solo;
	private final String USERNAME = "REAL USERNAME HERE";
	private final String PASSWORD = "REAL PASSWORD HERE";
	private final String WRONG_USERNAME = "x";
	private final String WRONG_PASSWORD = "y";
	public MainActivity_Test() {
		super(MainActivity.class);
	}

	public void setUp() throws Exception {
		solo = new Solo(getInstrumentation(), getActivity());
	}

	/**
	 * If the credentials dialog returns empty string, it should be shown again 
	 * 
	 * @throws Exception
	 */
	@Smoke
	public void testEmptyCredentials() throws Exception {
		
		solo.clickLongOnText("OK");
		boolean expected = false;
		boolean actual = solo.searchText("TWEETS");
		assertEquals("Word TWEETS was found", expected, actual);
	}
	
	/**
	 * If credentials dialog returns wrong credentials, it should be shown again 
	 * 
	 * @throws Exception
	 */
	@Smoke
	public void testWrongCredentials() throws Exception {
		
		solo.typeText(0, WRONG_USERNAME);
		solo.typeText(1, WRONG_PASSWORD);
		solo.clickLongOnText("OK");
		synchronized (solo) {
			solo.wait(10000);
		}
		boolean expected = false;
		boolean actual = solo.searchText("TWEETS");
		assertEquals("Word TWEETS was found", expected, actual);
	}
	
	/**
	 * If credentials dialog returns the right credentials, the string "TWEETS" should be shown in title 
	 * 
	 * @throws Exception
	 */
	@Smoke
	public void testUseCredentials() throws Exception {
		
		solo.typeText(0, USERNAME);
		solo.typeText(1, PASSWORD);
		solo.clickLongOnText("OK");
		synchronized (solo) {
			solo.wait(10000);
		}
		boolean expected = true;
		boolean actual = solo.searchText("TWEETS");
		assertEquals("Word TWEETS was not found", expected, actual);
	}
}